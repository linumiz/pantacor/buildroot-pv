PANTAVISOR_SITE =  https://gitlab.com/pantacor/pantavisor.git
PANTAVISOR_VERSION = 1294089a0115a70ffd6745af78e95787767e20ee
PANTAVISOR_SITE_METHOD = git
PANTAVISOR_INSTALL_STAGING = NO
PANTAVISOR_INSTALL_TARGET =YES
PANTAVISOR_CONF_OPTS = -DBUILD_DEMOS=ON
PANTAVISOR_DEPENDENCIES = libthttp pvlogger picohttpparser

define PANTAVISOR_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 755 ${BUILD_DIR}/pantavisor-$(PANTAVISOR_VERSION)/init $(TARGET_DIR)/sbin/
	$(INSTALL) -D -m 755 ${BUILD_DIR}/pantavisor-$(PANTAVISOR_VERSION)/init $(TARGET_DIR)/init
	$(INSTALL) -D -m 755 ${BUILD_DIR}/pantavisor-$(PANTAVISOR_VERSION)/libpanta.so $(TARGET_DIR)/lib
	$(INSTALL) -D -m 755 ${BUILD_DIR}/pantavisor-$(PANTAVISOR_VERSION)/parser/libparser.so $(TARGET_DIR)/lib
	$(INSTALL) -D -m 755 ${BUILD_DIR}/pantavisor-$(PANTAVISOR_VERSION)/plugins/libpv_lxc.so $(TARGET_DIR)/lib/libpv_lxc.so
	$(INSTALL) -D -m 755 ${BUILD_DIR}/pantavisor-$(PANTAVISOR_VERSION)/ph_logger/libph_logger.so $(TARGET_DIR)/lib
	$(INSTALL) -D -m 755 ${BUILD_DIR}/pantavisor-$(PANTAVISOR_VERSION)/utils/libutils.so $(TARGET_DIR)/lib
	$(INSTALL) -D -m 755 ${BUILD_DIR}/pantavisor-$(PANTAVISOR_VERSION)/scripts/JSON.sh $(TARGET_DIR)/lib/pv/JSON.sh	
	$(INSTALL) -D -m 755 ${BUILD_DIR}/pantavisor-$(PANTAVISOR_VERSION)/scripts/pv_e2fsgrow $(TARGET_DIR)/lib/pv/pv_e2fsgrow	
	$(INSTALL) -D -m 755 ${BUILD_DIR}/pantavisor-$(PANTAVISOR_VERSION)/scripts/hooks_lxc-mount.d/export.sh \
																	$(TARGET_DIR)/lib/pv/hooks_lxc-mount.d/export.sh	
	$(INSTALL) -D -m 755 ${BUILD_DIR}/pantavisor-$(PANTAVISOR_VERSION)/scripts/volmount/dm $(TARGET_DIR)/lib/pv/volmount/dm
	$(INSTALL) -D -m 755 $(BR2_EXTERNAL_pantavisor_PATH)/configs/mandatory/resolv.conf $(TARGET_DIR)/etc/resolv.conf
	$(INSTALL) -D -m 755 $(BR2_EXTERNAL_pantavisor_PATH)/configs/mandatory/init/lxc-net.conf.in $(TARGET_DIR)/etc/init/lxc-net.conf.in
	$(INSTALL) -D -m 755 $(BR2_EXTERNAL_pantavisor_PATH)/configs/mandatory/init/lxc-instance.conf $(TARGET_DIR)/etc/init/lxc-instance.conf
	$(INSTALL) -D -m 755 $(BR2_EXTERNAL_pantavisor_PATH)/configs/mandatory/init/lxc.conf $(TARGET_DIR)/etc/init/lxc.conf
	$(INSTALL) -D -m 755 $(BR2_EXTERNAL_pantavisor_PATH)/configs/mandatory/e2scrub.conf $(TARGET_DIR)/etc/e2scrub.conf
	$(INSTALL) -D -m 755 $(BR2_EXTERNAL_pantavisor_PATH)/configs/mandatory/pantavisor.config $(TARGET_DIR)/etc/pantavisor.config
endef

$(eval $(cmake-package))
